#include <stdio.h>

int main (int argc, char *argv[]) {
  int i;
  for (i = 1; i < 101; i++) {
    ( ((i%3)==0) || ((i%5)==0) ) ? \
    printf("%s\n", ((i%15)==0) ? "fizzbuzz" : ((i%3)==0) ? "fizz" : "buzz" ) : \
    printf("%d\n", i);
  }

  return 0;
}
